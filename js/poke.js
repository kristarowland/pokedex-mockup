$("#loading-dots").hide();

$("#searchBttn").click(function(e) {
  var text = $("#input").val().toLowerCase();
  getPokemon(text);
});

function getIdNumber() {
  var identifier = $("#pokeId").text();
  var idNumber = identifier.substr(4);
  return parseInt(idNumber);
}

$("#rightIdArrow").click(function(e) {
  console.log(getIdNumber());
  getPokemon(getIdNumber() + 1);
});

$("#leftIdArrow").click(function(e) {
  if (getIdNumber() === 0) {
    return;
  }
  getPokemon(getIdNumber() - 1);
});

function getPokemon(id) {
  $(".pokeData").empty();
  $("#loading-dots").show();

  var pokemonType = $.get("https://pokeapi.co/api/v2/pokemon/" + id);
  var pokemonSpecies = $.get("https://pokeapi.co/api/v2/pokemon-species/" + id);

  $.when(pokemonType, pokemonSpecies).done(showJson);
}

function showJson(typeResponse, speciesResponse) {
  $("#loading-dots").hide();
  var typeData = typeResponse[0];
  var speciesData = speciesResponse[0];

  console.log(typeData);
  console.log(speciesData);

  $("#name").text("Name: " + typeData.name);
  $("#hp").text("HP: " + typeData.stats[5].base_stat);
  $("#habitat").text("Habitat: " + speciesData.habitat.name);
  $("#speed").text("Speed: " + typeData.stats[0].base_stat);
  $("#attack").text("Attack: " + typeData.stats[4].base_stat);
  $("#defense").text("Defense: " + typeData.stats[3].base_stat);
  $("#pokeId").text("no. " + typeData.id);
  $("#pokeSprite").html("<img id=pokeImg src='" + typeData.sprites.front_default + "'>")

  for (var i = 0; i < typeData.types.length; i++) {
    $("#info0").append("<p class='pokeData'> Type: " + typeData.types[i].type.name + "</p>");
  }

  for (var a = 0; a < typeData.abilities.length; a++) {
    $("#info1").append("<p class='pokeData'> Abilities: " + typeData.abilities[a].ability.name + "</p>");
  }
}

$("#clearBttn").click(function(e) {
  $(".pokeData").empty();
});

var pageId = 0;

function updatePage()
{
  $(".pokeInfo").hide();
  $("#info" + pageId).show();
}

$("#leftDataArrow").click(function(e) {
  if (pageId === 0) {
    return;
  }
  pageId = pageId - 1; 
  updatePage();
});

$("#rightDataArrow").click(function(e) {
  if (pageId === 2) {
    return;
  }
  pageId = pageId + 1;
  updatePage();
});

updatePage();
















